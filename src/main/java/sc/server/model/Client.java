package sc.server.model;

public class Client {
    private Integer id = null;
    private Integer balance;
    private Account account;

    public Client(int id, int balance, Account account) {
        this.id = id;
        this.balance = balance;
        this.account = account;
    }

    public Client(int balance, Account account) {
        this.balance = balance;
        this.account = account;
    }

    public Integer getId() {
        return id;
    }

    public Integer getBalance() {
        return balance;
    }

    public Account getAccount() {
        return account;
    }

    public boolean isCreated() {
        return id != null;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Client" +
                " id: " + id +
                ", balance: " + balance +
                ", (" + account.toString() + ")";
    }
}
