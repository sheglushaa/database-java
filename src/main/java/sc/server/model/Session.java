package sc.server.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.BitSet;

public class Session {
    private String token;
    private Integer accountId;
    private InetAddress ip;

    public Session(String token, Integer accountId, byte[] ip) {
        this.token = token;
        this.accountId = accountId;
        this.ip = getInetAddress(ip);

    }

    private InetAddress getInetAddress(byte[] ip) {
        try {
          return InetAddress.getByAddress(ip);
        } catch (UnknownHostException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Session " +
                "token: " + token +
                ", accountId: " + accountId +
                ", ip: " + ip.toString();
    }
}
