package sc.server.model;


public class Position {
    public static final Position DEFAULT = new Position("Не установлено", 0);

    private Integer id = null;
    private String name;
    private Integer salary;

    public Position(int id, String name, int salary){
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public Position(String name, int salary){
        this.name = name;
        this.salary = salary;
    }

    public Integer getId() { return id; }

    public String getName() { return name; }

    public Integer getSalary() { return salary; }

    public boolean isCreated() { return id != null; }

    @Override
    public String toString() {
        return "Position " +
                "id: " + id +
                ", name: " + name +
                ", salary: " + salary;
    }
}
