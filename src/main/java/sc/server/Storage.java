package sc.server;

import sc.server.model.Section;
import sc.server.repository.*;

public class Storage {
    public static AccountRepository getAccountRepository() {
        return accountRepository;
    }
    public static ClientRepository getClientRepository() { return clientRepository; }
    public static EmployerRepository getEmployerRepository() { return employerRepository; }
    public static PositionRepository getPositionRepository() {
        return positionRepository;
    }
    public static SessionRepository getSessionRepository() {
        return sessionRepository;
    }
    public static SectionRepository getSectionRepository() {
        return sectionRepository;
    }
    public static ClassRepository getClassRepository() { return classRepository; }

    private static final AccountRepository accountRepository = new AccountRepository();
    private static final ClientRepository clientRepository = new ClientRepository();
    private static final EmployerRepository employerRepository = new EmployerRepository();
    private static final PositionRepository positionRepository = new PositionRepository();
    private static final SessionRepository sessionRepository = new SessionRepository();
    private static final SectionRepository sectionRepository = new SectionRepository();
    private static final ClassRepository classRepository = new ClassRepository();

}
