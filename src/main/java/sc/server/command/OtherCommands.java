package sc.server.command;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import sc.server.core.DatabaseManager;
import sc.server.model.Class;
import sc.server.model.Client;

import java.util.List;

@ShellComponent
public class OtherCommands {
    @ShellMethod("Список самых популярных секций за последнее количество месяцев")
    public void getmostpopularsections(
            @ShellOption("Количество месяцев") int monthCount,
            @ShellOption("Максимальное количество секций") int sectionsLimit
    ) {
        if (monthCount < 1) {
            System.out.println("Количество месяцев должно быть не меньше 1");
            return;
        }

        if (sectionsLimit < 1) {
            System.out.println("Количество секций должно быть не меньше 1");
            return;
        }

        JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();
        jdbcTemplate.query("CALL SelectMostPopularSections(?, ?)", new Object[]{monthCount, sectionsLimit}, resultSet -> {
            System.out.println(
                "Секция: " + resultSet.getString("name") + " - " +
                "Количество записей: " + resultSet.getInt("count")
            );
        });
    }

}
