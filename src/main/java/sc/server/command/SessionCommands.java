package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Session;
import sc.server.repository.SessionRepository;

import java.util.List;

@ShellComponent
public class SessionCommands {
    SessionRepository sessionRepository = Storage.getSessionRepository();

    @ShellMethod("Get all active sessions")
    public void getallsessions() {
        List<Session> sessions = sessionRepository.findAll();
        for (Session session : sessions)
            System.out.println(session);
    }

    @ShellMethod("Reset all sessions")
    public void resetallsessions() {
       int count = sessionRepository.deleteAll();
        System.out.println("Deleted: " + count + " sessions");
    }
}
