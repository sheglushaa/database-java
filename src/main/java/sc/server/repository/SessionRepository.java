package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import sc.server.core.DatabaseManager;
import sc.server.model.Position;
import sc.server.model.Session;

import java.sql.ResultSet;
import java.util.List;

public class SessionRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Session> ROW_MAPPER_SESSION = (ResultSet resultSet, int rowNum) -> new Session(
            resultSet.getString("token"),
            resultSet.getInt("account_id"),
            resultSet.getBytes("ip")
    );

    public List<Session> findAll() {
        return jdbcTemplate.query("CALL SelectAllSessions()", ROW_MAPPER_SESSION);
    }

    public int deleteAll() {return jdbcTemplate.update("CALL DeleteAllSessions()"); }
}
