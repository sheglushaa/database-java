package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.*;
import sc.server.model.Class;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClassRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Class> ROW_MAPPER_CLASS = (ResultSet resultSet, int rowNum) -> {
        Section section = new Section(
            resultSet.getInt("section_id"),
            resultSet.getString("section_name"),
            resultSet.getInt("section_max_clients")
        );

        Account account = new Account(
            resultSet.getInt("id"),
            resultSet.getString("login"),
            resultSet.getString("password"),
            resultSet.getString("salt"),
            resultSet.getString("first_name"),
            resultSet.getString("second_name"),
            resultSet.getString("phone_number")
        );

        Employer employer = new Employer(
            resultSet.getInt("employer_id"),
            null,
            account
        );

        return new Class(
            resultSet.getInt("class_id"),
            resultSet.getDate("class_date"),
            section,
            employer
        );
    };

    public List<Class> findAll() {
        return jdbcTemplate.query("CALL SelectAllClasses()", ROW_MAPPER_CLASS);
    }

    public Class findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("CALL SelectClassById(?)", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public List<Class> findBySection(Section section) {
        try {
            return jdbcTemplate.query("CALL SelectClassesBySection(?)", new Object[]{section.getId()}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public List<Class> findByEmployer(Employer employer) {
        try {
            return jdbcTemplate.query("CALL SelectClassesByEmployer(?)", new Object[]{employer.getId()}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public List<Class> findByClient(Client client) {
        try {
            Integer id = client.getId();
            return jdbcTemplate.query("CALL SelectClassesByClient(?)", new Object[]{id}, ROW_MAPPER_CLASS);
        } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public Class create(Class classToCreate) {
        if (classToCreate.isCreated()) {
            return null;
        }

        Date date = classToCreate.getDate();
        Section section = classToCreate.getSection();
        Employer employer = classToCreate.getEmployer();

        Integer id = jdbcTemplate.queryForObject("CALL CreateClass(?, ?, ?)",
            new Object[]{new java.sql.Timestamp(date.getTime()), section.getId(), employer.getId()},
            Integer.class
        );

        return id == null ? null : new Class(id, date, section, employer);
    }

    public boolean create(Class classTo, Client client) {
        try {
            jdbcTemplate.update("CALL CreateClassClient(?, ?)", classTo.getId(), client.getId());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
