package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Section;
import sc.server.repository.SectionRepository;

import java.util.List;

@ShellComponent
public class SectionCommands {
    SectionRepository sectionRepository = Storage.getSectionRepository();

    @ShellMethod("Add section")
    public void addsection(String name, int maxClients) {
        Section createdSection = sectionRepository.create(new Section(name, maxClients));
        if (createdSection == null) {
            System.out.println("Can not create new section");
            return;
        }

        System.out.println("Created: " + createdSection.toString());
    }

    @ShellMethod("Delete section")
    public void deletesection(Integer id) {
        Section section = sectionRepository.findById(id);
        if (section == null) {
            System.out.println("Section not found");
            return;
        }

        sectionRepository.delete(section);
        System.out.println("Successfully deleted " + section.toString());
    }

    @ShellMethod("Get all sections")
    public void getallsections() {
        List<Section> sections = sectionRepository.findAll();
        for (Section section : sections)
            System.out.println(section);
    }
}
