package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Account;
import sc.server.model.Class;
import sc.server.model.Client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class ClientRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Client> ROW_MAPPER_CLIENT = (ResultSet resultSet, int rowNum) -> {
        Account account = new Account(
                resultSet.getInt("id"),
                resultSet.getString("login"),
                resultSet.getString("password"),
                resultSet.getString("salt"),
                resultSet.getString("first_name"),
                resultSet.getString("second_name"),
                resultSet.getString("phone_number")
        );

        Client client = new Client(
                resultSet.getInt("client_id"),
                resultSet.getInt("client_balance"),
                account
        );
        account.setClient(client);
        return client;
    };

    public List<Client> findAll() {
        try {
            return jdbcTemplate.query(
                    "CALL SelectAllClients()", ROW_MAPPER_CLIENT);
         } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public Client findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("CALL SelectClientById(?)", new Object[]{id}, ROW_MAPPER_CLIENT);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public List<Client> findByClass(Class classBy) {
        try {
            Integer id = classBy.getId();
            return jdbcTemplate.query(
                "CALL SelectClientByClass(?)", new Object[]{id}, ROW_MAPPER_CLIENT);
        } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public boolean save(Client client) {
        if (!client.isCreated()) {
            return false;
        }
        return jdbcTemplate.update("CALL UpdateClient(?, ?)",
               client.getBalance(), client.getId()
        ) == 1;
    }

    public Client create(Client client) {
        if (client.isCreated())
            return null;

        int balance = client.getBalance();
        Account account = client.getAccount();

        Integer id = jdbcTemplate.queryForObject("CALL CreateClient(?, ?)",
                new Object[]{balance, account.getId()},
                Integer.class
        );
;
        return id == null ? null : new Client(id, balance, account);
    }






//        private RowMapper<Client> ROW_MAPPER = (ResultSet resultSet, int rowNum) -> {
//            return new Client(
//                    resultSet.getInt("id"),
//                    resultSet.getString("first_name"),
//                    resultSet.getString("second_name"),
//                    resultSet.getString("phone_number"),
//                    resultSet.getInt("balance")
//                    //  resultSet.getInt("account")
//            );
//        }
//
//        public List<Client> findAll() {
//            return jdbcTemplate.query("SELECT * FROM clients", ROW_MAPPER);
//        }
//
//        public Client findById(int id) {
//            Client client = null;
//
//            try {
//                client = jdbcTemplate.queryForObject("SELECT * FROM clients WHERE id = ?", new Object[]{id}, ROW_MAPPER);
//            } catch (Exception dataAccessException) {
//                System.out.println("Can't find client with ID: " + id);
//            }
//
//            return client;
//        }
//
       // Вернет 1, если аккаунт сохранен, 0, если нет

}


