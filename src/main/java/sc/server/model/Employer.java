package sc.server.model;

public class Employer {
    private Integer id = null;
    private Position position;
    private Account account;

    public Employer(int id, Position position, Account account) {
        this.id = id;
        this.position = position;
        this.account = account;
    }

    public Employer(Position position, Account account) {
        this.position = position;
        this.account = account;
    }

    public Integer getId() { return id; }

    public Position getPosition() { return position; }

    public Account getAccount() { return account; }

    public boolean isCreated() {
        return id != null;
    }

    @Override
    public String toString() {
        return "Employer" +
                " id: " + id + "\n" +
                (position != null ? " - " + position.toString() + "\n" : "") +
                " - " + account.toString();
    }
}
