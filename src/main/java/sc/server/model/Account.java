package sc.server.model;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

public class Account {
    private Integer id = null;
    private String login;
    private String passwordHash;
    private String passwordSalt;
    private String firstName;
    private String secondName;
    private String phoneNumber;
    private Client client = null;
    private Employer employer = null;

    // для загрузки из базы
    public Account(int id, String login, String passwordHash, String passwordSalt, String firstName, String secondName, String phoneNumber) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;
        this.passwordSalt = passwordSalt;
        this.firstName = firstName;
        this.secondName = secondName;
        this.phoneNumber = phoneNumber;
    }

//    public Account(int id, String login, String passwordHash, String passwordSalt, String firstName, String secondName,
//                   String phoneNumber, Client client, Employer employer) {
//        this.id = id;
//        this.login = login;
//        this.passwordHash = passwordHash;
//        this.passwordSalt = passwordSalt;
//        this.firstName = firstName;
//        this.secondName = secondName;
//        this.phoneNumber = phoneNumber;
//        this.client = client;
//        this.employer = employer;
//    }

    // создание нового и запись в базу
    public Account(String login, String password, String firstName, String secondName, String phoneNumber) {
        this.login = login;
        this.firstName = firstName;
        this.secondName = secondName;
        this.phoneNumber = phoneNumber;

        this.setPassword(password);
    }

    public static boolean isValidName(String name) {
        return name.matches("[a-zA-Zа-яА-Я]+\\.?");
    }

    public static boolean isValidPhoneNumber(String phoneNumber) { return StringUtils.isNumeric(phoneNumber); }

    public Integer getId() {
        return id;
    }

    public boolean isClient() {
        return this.client != null;
    }

    public boolean isEmployer() {
        return this.employer != null;
    }

    public Employer getEmployer() { return employer; }

    public Client getClient() { return client; }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public boolean isPasswordValid(String password) {
        return DigestUtils.sha512Hex(password + passwordSalt).equalsIgnoreCase(passwordHash);
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPassword(String password) {
        this.passwordSalt = RandomStringUtils.randomAlphabetic(8);
        this.passwordHash = DigestUtils.sha512Hex(password + this.passwordSalt);
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public boolean isCreated() {
        return id != null;
    }

    @Override
    public String toString() {
        return "Account " +
                "id: " + id +
                ", name: " + firstName + " " + secondName +
                ", phone number: " + phoneNumber +
                ", login: " + login +
                ", password hash: " + passwordHash +
                ", password salt: " + passwordSalt
                ;
    }
}
