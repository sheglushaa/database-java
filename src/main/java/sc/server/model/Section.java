package sc.server.model;


//id , name , max_clients
public class Section {
    private Integer id = null;
    private String name;
    private int maxClients;

    public Section (int id, String name, int maxClients) {
        this.id = id;
        this.name = name;
        this.maxClients = maxClients;
    }

    public Section (String name, int maxClients) {
        this.name = name;
        this.maxClients = maxClients;
    }

    public Integer getId() { return id; }

    public String getName() { return name; }

    public int getMaxClients() { return maxClients; }

    public boolean isCreated() { return id != null; }

    @Override
    public String toString() {
        return "Section " +
                "id: " + id +
                ", name: " + name +
                ", max clients: " + maxClients;
    }
}
