package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Position;
import sc.server.model.Section;
import sc.server.repository.PositionRepository;

import java.util.List;

@ShellComponent
public class PositionCommands {
    private PositionRepository positionRepository = Storage.getPositionRepository();

    @ShellMethod("Add new position")
    public void addposition(String name, Integer salary) {
        if (salary < 0) {
            System.out.println("Incorrect salary");
        }

        Position createdPosition = positionRepository.create(new Position(name, salary));
        if (createdPosition == null) {
            System.out.println("Can not create new position");
            return;
        }
        System.out.println("Create new position: " + createdPosition.toString());
    }

    @ShellMethod("Get all positions")
    public void getallpositions() {
        List<Position> positions = positionRepository.findAll();
        for (Position position : positions)
            System.out.println(position);
    }

}
