package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Account;
import sc.server.model.Position;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class PositionRepository {

    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Position> ROW_MAPPER_POSITION = (ResultSet resultSet, int rowNum) -> new Position(
            resultSet.getInt("id"),
            resultSet.getString("name"),
            resultSet.getInt("salary")
    );

    public Position findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("CALL SelectPositionById(?)", new Object[]{id}, ROW_MAPPER_POSITION);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public List<Position> findAll() {
        try {
            return jdbcTemplate.query(
                    "CALL SelectAllPositions()", ROW_MAPPER_POSITION);
        } catch (Exception dataAccessException) {
            return new ArrayList<>();
        }
    }

    public Position create(Position position) {
        if (position.isCreated()) {
            return null;
        }

        String name = position.getName();
        Integer salary = position.getSalary();

        Integer id = jdbcTemplate.queryForObject("CALL CreatePosition(?, ?)", new Object[]{name, salary} , Integer.class);

        return id == null ? null : new Position(id, name, salary);
    }
}
