package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Account;
import sc.server.model.Class;
import sc.server.model.Client;
import sc.server.repository.AccountRepository;
import sc.server.repository.ClientRepository;

import java.util.List;

@ShellComponent
public class ClientCommands {
    private ClientRepository clientRepository = Storage.getClientRepository();
    private AccountRepository accountRepository = Storage.getAccountRepository();

    @ShellMethod("Get all clients")
    public void getallclients() {
        List<Client> clients = clientRepository.findAll();
        for (Client client : clients)
            System.out.println(client.toString());
    }

    @ShellMethod("Get client by id")
    public void getclient(Integer id) {
        Client client = clientRepository.findById(id);
        if (client == null) {
            System.out.println("Client not found");
            return;
        }

        System.out.println(client.toString());
    }

    @ShellMethod("Set balance by id")
    public void setbalance(Integer id, Integer balance) {
        if (balance < 0) {
            System.out.println("Incorrect balance");
            return;
        }


        Client client = clientRepository.findById(id);
        if (client == null) {
            System.out.println("Client not found");
            return;
        }

        client.setBalance(balance);
        clientRepository.save(client);
        System.out.println("Balance set for: " + client.toString());
    }

    @ShellMethod("Increase balance by id")
    public void increasebalance(Integer id, Integer value) {

        if (value < 0) {
            System.out.println("Incorrect value");
            return;
        }

        Client client = clientRepository.findById(id);
        if (client == null) {
            System.out.println("Client not found");
            return;
        }

        client.setBalance(client.getBalance() + value);
        clientRepository.save(client);
        System.out.println("New balance set for: " + client.toString());
    }

    @ShellMethod("Decrease balance by id")
    public void decreasebalance(Integer id, Integer value) {
        if (value < 0) {
            System.out.println("Incorrect value");
            return;
        }

        Client client = clientRepository.findById(id);
        if (client == null) {
            System.out.println("Client not found");
            return;
        }

        if (client.getBalance() < value) {
            System.out.println("Balance has no " + value);
            return;
        }

        client.setBalance(client.getBalance() - value);
        clientRepository.save(client);
        System.out.println("New balance set for: " + client.toString());
    }

    @ShellMethod("Add new client")
    public void addclient(String login, String password, String firstname, String secondname, String phonenumber) {
        if (!Account.isValidName(firstname))
        {
            System.out.println("First name is incorrect");
            return;
        }
        if (!Account.isValidName(secondname)) {
            System.out.println("Second name is incorrect");
            return;
        }
        if (!Account.isValidPhoneNumber(phonenumber)) {
            System.out.println("Phone number is incorrect");
            return;
        }
        Account account = accountRepository.findByLogin(login);
        if (account != null) {
            System.out.println("This login is already used");
            return;
        }

        Account createdAccount = accountRepository.create(new Account(login, password, firstname, secondname, phonenumber));
        if (createdAccount == null) {
            System.out.println("Can not create account for client");
            return;
        }

        Client client = clientRepository.create(new Client(0, createdAccount));
        if (client == null) {
            System.out.println("Can not create client");
            return;
        }

        System.out.println("Created client with id: " + client.getId() + " (Account id: " + createdAccount.getId() + ")");
    }
}
