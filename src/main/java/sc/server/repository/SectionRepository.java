package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Section;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class SectionRepository {
private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Section> ROW_MAPPER_SECTION = (ResultSet resultSet, int rowNum) -> new Section(
            resultSet.getInt("id"),
            resultSet.getString("name"),
            resultSet.getInt("max_clients")
    );

    public Section findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("CALL SelectSectionBbyId(?)", new Object[]{id}, ROW_MAPPER_SECTION);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public List<Section> findAll() { return jdbcTemplate.query("CALL SelectAllSections()", ROW_MAPPER_SECTION);
    }

    public Section create(Section section) {
        if (section.isCreated()) {
            return null;
        }

        Integer id = jdbcTemplate.queryForObject(
                    "CALL CreateSection(?, ?)", new Object[]{section.getName(), section.getMaxClients()}, Integer.class);

        return id == null ? null : new Section(id, section.getName(), section.getMaxClients());
    }

    public int delete(int id) {
        return jdbcTemplate.update("CALL DeleteSectionById(?)", id);
    }

    public int delete(Section section) { return delete(section.getId()); }
}
