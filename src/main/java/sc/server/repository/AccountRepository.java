package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Account;
import sc.server.model.Client;
import sc.server.model.Employer;
import sc.server.model.Position;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;


public final class AccountRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Account> ROW_MAPPER_ACCOUNT_ONLY = (ResultSet resultSet, int rowNum) -> new Account(
        resultSet.getInt("id"),
        resultSet.getString("login"),
        resultSet.getString("password"),
        resultSet.getString("salt"),
        resultSet.getString("first_name"),
        resultSet.getString("second_name"),
        resultSet.getString("phone_number")
    );

    private RowMapper<Account> ROW_MAPPER_ACCOUNT = (ResultSet resultSet, int rowNum) -> {
        Account account = new Account(
            resultSet.getInt("id"),
            resultSet.getString("login"),
            resultSet.getString("password"),
            resultSet.getString("salt"),
            resultSet.getString("first_name"),
            resultSet.getString("second_name"),
            resultSet.getString("phone_number")
        );

        int clientId = resultSet.getInt("client_id");
        if (clientId >= 1) {
            int clientBalance = resultSet.getInt("client_balance");
            account.setClient(new Client(clientId, clientBalance, account));
        }

        int employerId = resultSet.getInt("employer_id");
        if (employerId >= 1) {
            Position position = Position.DEFAULT;

            int positionId = resultSet.getInt("position_id");
            if (positionId >= 1) {
                position = new Position(
                    resultSet.getInt("position_id"),
                    resultSet.getString("position_name"),
                    resultSet.getInt("position_salary")
                );
            }

            account.setEmployer(
                new Employer(
                    employerId,
                    position,
                    account
                )
            );
        }

        return account;
    };


    public List<Account> findAll() {
        return jdbcTemplate.query("CALL SelectAllAccounts()", ROW_MAPPER_ACCOUNT_ONLY);
    }
    
    public Account findById(int id, boolean isAccountOnly) {
        RowMapper<Account> rowMapper;
        String query;

        if (isAccountOnly) {
            rowMapper = ROW_MAPPER_ACCOUNT_ONLY;
            query = "CALL SelectAccountOnlyById(?)";
        } else {
            rowMapper = ROW_MAPPER_ACCOUNT;
            query = "CALL SelectAccountById(?)";
        }

        try {
            return jdbcTemplate.queryForObject(query, new Object[]{id}, rowMapper);
        } catch (Exception dataAccessException) {
            return null;
        }

    }

    public Account findById(int id) {
        return findById(id, true);
    }

    // Вернет 1, если аккаунт сохранен, 0, если нет
    public void save(Account account) {
        if (!account.isCreated()) {
            this.create(account);
            return;
        }

        jdbcTemplate.update("CALL UpdateAccount(?,?,?,?)",
            account.getLogin(), account.getPasswordHash(), account.getPasswordSalt(), account.getId()
        );
    }

    public Account create(Account account) {
        if (account.isCreated()) {
            return null;
        }

        String login = account.getLogin();
        String passwordHash = account.getPasswordHash();
        String passwordSalt = account.getPasswordSalt();
        String firstName = account.getFirstName();
        String secondName = account.getSecondName();
        String phoneNumber = account.getPhoneNumber();

        Integer id = jdbcTemplate.queryForObject("CALL CreateAccount(?, ?, ?, ?, ?, ?)",
                new Object[]{login, passwordHash, passwordSalt, firstName, secondName, phoneNumber},
                Integer.class
        );

        return id == null ? null : new Account(id, login, passwordHash, passwordSalt, firstName, secondName, phoneNumber);
    }

    public Account findByLogin(String login, boolean isAccountOnly) {
        RowMapper<Account> rowMapper;
        String query;

        if (isAccountOnly) {
            rowMapper = ROW_MAPPER_ACCOUNT_ONLY;
            query = "CALL SelectAccountOnlyByLogin(?)";
        } else {
            rowMapper = ROW_MAPPER_ACCOUNT;
            query = "CALL SelectAccountByLogin(?)";
        }

        try {
            return jdbcTemplate.queryForObject(query, new Object[]{login}, rowMapper);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

    public Account findByLogin(String login) {
        return findByLogin(login, true);
    }

    public int delete(int id) {
        return jdbcTemplate.update("CALL DeleteAccountById(?)", id);
    }

    public int delete(Account account) { return delete(account.getId()); }
}