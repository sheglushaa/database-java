package sc.server;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.shell.jline.PromptProvider;
import sc.server.core.DatabaseManager;

import java.sql.SQLException;

@SpringBootApplication
public class Main {
    public static void main(String[] args) throws SQLException {
        DatabaseManager.init();

        //DatabaseManager.getJdbcTemplate().execute("CALL DeleteOldClasses");

        SpringApplication app = new SpringApplication(Main.class);

        app.setBanner((environment, aClass, printStream) -> {
            System.out.println("SPORT CENTER CONSOLE MANAGEMENT");
            System.out.println("v1.0");
        });
        app.setBannerMode(Banner.Mode.CONSOLE);

        app.run(args);
    }

    @Bean
    public PromptProvider myPromptProvider() {
        return () -> new AttributedString("cmd:> ", AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN));
    }
}
