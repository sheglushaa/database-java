package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import sc.server.Storage;
import sc.server.model.Account;
import sc.server.model.Employer;
import sc.server.model.Position;
import sc.server.repository.AccountRepository;
import sc.server.repository.EmployerRepository;
import sc.server.repository.PositionRepository;

import java.util.List;

@ShellComponent
public class EmployerCommand {
    private EmployerRepository employerRepository = Storage.getEmployerRepository();
    private AccountRepository accountRepository = Storage.getAccountRepository();
    private PositionRepository positionRepository = Storage.getPositionRepository();

    @ShellMethod("Get all employees")
    public void getallemployees() {
        List<Employer> employees = employerRepository.findAll();
        for (Employer employer: employees)
            System.out.println(employer.toString());
    }

    @ShellMethod("Get employer by id")
    public void getemployer(@ShellOption(help = "Employer id") Integer id) {
        Employer employer = employerRepository.findById(id);
        if (employer == null) {
            System.out.println("Employer not found");
            return;
        }
        System.out.println(employer.toString());
    }

    @ShellMethod("Add new employer")
    public void addemployer(String login, String password, String firstname, String secondname, String phonenumber, Integer idPosition) {
        if (!Account.isValidName(firstname))
        {
            System.out.println("First name is incorrect");
            return;
        }
        if (!Account.isValidName(secondname)) {
            System.out.println("Second name is incorrect");
            return;
        }
        if (!Account.isValidPhoneNumber(phonenumber)) {
            System.out.println("Phone number is incorrect");
            return;
        }

        Position position = positionRepository.findById(idPosition);
        if (position == null) {
            System.out.println("Position not found");
            return;
        }

        Account account = accountRepository.findByLogin(login);
        if (account != null) {
            System.out.println("This login is already used");
            return;
        }
        Account createdAccount = accountRepository.create(new Account(login, password, firstname, secondname, phonenumber));
        if (createdAccount == null) {
            System.out.println("Can not create account for employer");
            return;
        }

        Employer employer = employerRepository.create(new Employer(position, createdAccount));
        if (employer == null) {
            System.out.println("Can not create employer");
            return;
        }
        System.out.println("Created employer with id: " + employer.getId() + " (Account id: " + createdAccount.getId() + ")");
    }
}
