package sc.server.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import sc.server.core.DatabaseManager;
import sc.server.model.Account;
import sc.server.model.Employer;
import sc.server.model.Position;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class EmployerRepository {
    private final JdbcTemplate jdbcTemplate = DatabaseManager.getJdbcTemplate();

    private RowMapper<Employer> ROW_MAPPER_EMPLOYER = (ResultSet resultSet, int rowNum) -> {
        Account account = new Account(
                resultSet.getInt("id"),
                resultSet.getString("login"),
                resultSet.getString("password"),
                resultSet.getString("salt"),
                resultSet.getString("first_name"),
                resultSet.getString("second_name"),
                resultSet.getString("phone_number")
        );
        Position position = new Position(
                resultSet.getInt("position_id"),
                resultSet.getString("position_name"),
                resultSet.getInt("salary")
        );
        Employer employer = new Employer(
                resultSet.getInt("employer_id"),
                position,
                account
        );
        account.setEmployer(employer);
        return employer;
    };

    public List<Employer> findAll() {
        return jdbcTemplate.query("CALL SelectAllEmployees()", ROW_MAPPER_EMPLOYER);
    }

    public Employer findById(Integer id) {
        try {
            return jdbcTemplate.queryForObject("CALL SelectEmployerById(?)", new Object[]{id}, ROW_MAPPER_EMPLOYER);
        } catch (Exception dataAccessException) {
            return null;
        }
    }

   public Employer create(Employer employer) {
       if (employer.isCreated())
           return null;

       Position position = employer.getPosition();
       Account account = employer.getAccount();

       Integer positionId = position.getId();
       Integer accountId = account.getId();

       Integer id = jdbcTemplate.queryForObject("CALL CreateEmployer(?, ?)", new Object[]{positionId, accountId},
               Integer.class);

       return id == null ? null : new Employer(id, position, account);
   }
}
