package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Class;
import sc.server.model.Client;
import sc.server.model.Employer;
import sc.server.model.Section;
import sc.server.repository.ClassRepository;
import sc.server.repository.ClientRepository;
import sc.server.repository.EmployerRepository;
import sc.server.repository.SectionRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@ShellComponent
public class ClassCommands {
    private SectionRepository sectionRepository = Storage.getSectionRepository();
    private EmployerRepository employerRepository = Storage.getEmployerRepository();
    private ClassRepository classRepository = Storage.getClassRepository();
    private ClientRepository clientRepository = Storage.getClientRepository();

    @ShellMethod("Get all classes")
    public void getallclasses() {
        List<Class> classes = classRepository.findAll();
        for (Class it : classes)
            System.out.println(it.toString());
    }

    @ShellMethod("Add new class")
    public void addclass(String dateStr, int sectionId, int employerId) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy/HH:mm");
        Date date;
        try {
            date = formatter.parse(dateStr);
            System.out.println(date);
            System.out.println(formatter.format(date));
        } catch (ParseException e) {
            System.out.println("Incorrect date");
            return;
        }

        Section section = sectionRepository.findById(sectionId);
        if (section == null) {
            System.out.println("Section not found");
            return;
        }

        Employer employer = employerRepository.findById(employerId);
        if (employer == null) {
            System.out.println("Employer not found");
            return;
        }

        Class createdClass = classRepository.create(new Class(date, section, employer));
        if (createdClass == null) {
            System.out.println("Can't create class");
            return;
        }

        System.out.println("Created class with id: " + createdClass.getId() + "(" + section.toString() + ", " + employer.toString() + ")");
    }

    @ShellMethod("Get section classes")
    public void getsectionclasses(int sectionId) {
        Section section = sectionRepository.findById(sectionId);
        if (section == null) {
            System.out.println("Section not found");
            return;
        }

        List<Class> classList = classRepository.findBySection(section);
        for (Class loadedClass : classList) {
            System.out.println(loadedClass);
        }
    }

    @ShellMethod("Get employer classes")
    public void getemployerclasses(int employerId) {
        Employer employer = employerRepository.findById(employerId);
        if (employer == null) {
            System.out.println("Employer not found");
            return;
        }

        List<Class> classList = classRepository.findByEmployer(employer);
        for (Class loadedClass : classList) {
            System.out.println(loadedClass);
        }
    }

    @ShellMethod("Get client classes")
    public void getclientclasses(int clientId) {
        Client client = clientRepository.findById(clientId);
        if (client == null) {
            System.out.println("Client not found");
            return;
        }

        List<Class> classList = classRepository.findByClient(client);
        for (Class loadedClass : classList) {
            System.out.println(loadedClass);
        }
    }

    @ShellMethod("Add client to class")
    public void addclienttoclass(int clientId, int classId) {
        Client client = clientRepository.findById(clientId);
        if (client == null) {
            System.out.println("Client not found");
            return;
        }

        Class classTo = classRepository.findById(classId);
        if (classTo == null) {
            System.out.println("Class not found");
            return;
        }

        List<Client> clients = clientRepository.findByClass(classTo);
        if (clients.size() > classTo.getSection().getMaxClients()) {
            System.out.println("Max clients for this class reached");
            return;
        }

        if (!classRepository.create(classTo, client)) {
            System.out.println("Client already added to class");
            return;
        }

        System.out.println("Added client to section (" + clients.size() + 1 + "/" + classTo.getSection().getMaxClients() + ")");
    }

    @ShellMethod("Get class clients")
    public void getclassclients(int classId) {
        Class loadedClass = classRepository.findById(classId);
        if (loadedClass == null) {
            System.out.println("Class not found");
            return;
        }

        List<Client> clients = clientRepository.findByClass(loadedClass);
        for (Client client : clients)
            System.out.println(client);
    }
}
