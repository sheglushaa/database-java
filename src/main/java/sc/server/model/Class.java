package sc.server.model;

import java.util.Date;

public class Class {
    public Integer id = null;
    private Date date;
    private Section section;
    private Employer employer;

    public Class(int id, Date date, Section section, Employer employer){
        this.id = id;
        this.date = date;
        this.section = section;
        this.employer = employer;
    }

    public Class(Date date, Section section, Employer employer){
        this.date = date;
        this.section = section;
        this.employer = employer;
    }

    public Integer getId() { return id; }

    public Date getDate() { return date; }

    public Section getSection() { return section; }

    public Employer getEmployer() { return employer; }

    public boolean isCreated() { return id != null; }

    @Override
    public String toString() {
        return "Class " +
                "id: " + id +
                ", date: " + date +
                ", section: " + section.toString() +
                ", employer: " + employer.toString();
    }
}
