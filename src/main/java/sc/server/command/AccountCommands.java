package sc.server.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import sc.server.Storage;
import sc.server.model.Account;
import sc.server.model.Client;
import sc.server.model.Employer;
import sc.server.model.Position;
import sc.server.repository.AccountRepository;

import java.util.List;

@ShellComponent()
public class AccountCommands {
    private AccountRepository accountRepository = Storage.getAccountRepository();

    @ShellMethod("Get account by id")
    public void getaccount(Integer id) {
        Account account = accountRepository.findById(id);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        System.out.println(account.toString());
    }

    @ShellMethod("Get all accounts")
    public void getallaccounts() {
        List<Account> accounts = accountRepository.findAll();
        for (Account account : accounts)
            System.out.println(account.toString());
    }

    @ShellMethod("Set account password by id")
    public void setaccountpassword(Integer id, String password) {
        Account account = accountRepository.findById(id);

        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        account.setPassword(password);
        accountRepository.save(account);
        System.out.println("Password set for " + account.toString());
    }

    @ShellMethod("Delete account by id")
    public void deleteaccount(Integer id) {
        Account account = accountRepository.findById(id);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        accountRepository.delete(account);
        System.out.println("Successfully deleted " + account.toString());
    }

    @ShellMethod("Get account type by id")
    public void getaccounttype(Integer id) {
        Account account = accountRepository.findById(id, false);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }

        if (account.isClient() && account.isEmployer()) {
            System.out.println("Account is client and employer");
            return;
        }

        if (account.isClient()) {
            System.out.println("Account is client");
            return;
        }

        if (account.isEmployer()) {
            System.out.println("Account is employer");
            return;
        }

        System.out.println("Account: Who am I???");
    }

    @ShellMethod("Get account info")
    public void getaccountinfo(Integer id) {
        Account account = accountRepository.findById(id, false);
        if (account == null) {
            System.out.println("Account not found");
            return;
        }
        System.out.println(account.toString());
        if (account.isEmployer()) {
            Employer employer = account.getEmployer();
            Position position = employer.getPosition();
            System.out.println("- Employer info: id: " + employer.getId() + ", position: " + position.getName()
            + "(id: " + position.getId() + ")" + ", salary: " + position.getSalary());
        }
        if (account.isClient()) {
            Client client = account.getClient();
            System.out.println(" - Client info: id: " +client.getId() + ", balance: " + client.getBalance());
        }
    }

}
